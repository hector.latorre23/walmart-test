import logging


from flask import Flask, has_request_context, request, render_template
from flask_cors import CORS
from flask_mongoengine import MongoEngine

db = MongoEngine()
cors = CORS()


def create_app(app_settings):
    config_logger()
    app = Flask(__name__)

    app.config.from_object(app_settings)

    cors.init_app(app)
    db.init_app(app)
    
    return app
