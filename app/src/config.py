import os
import urllib


class BaseConfig:
    """ Base configuration """

    DEBUG = True
    TESTING = False
    MONGO_DB_URI = ""


class DevelopmentConfig(BaseConfig):
    """ Development configuration """

    ENV = "development"



class TestingConfig(BaseConfig):
    """ Testing configuration """

    TESTING = True


class StagingConfig(BaseConfig):
    """ Staging configuration """

    ENV = "stage"



class ProductionConfig(BaseConfig):
    """ Production configuration """

    ENV = "production"

